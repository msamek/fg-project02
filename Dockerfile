#Builder
FROM maven:3.6.3-openjdk-8-slim as builder
RUN mkdir -p /build
WORKDIR /build
COPY pom.xml /build
RUN mvn -B dependency:resolve dependency:resolve-plugins
COPY src /build/src
RUN mvn package

#Runtime
FROM maven:3.6.3-openjdk-8-slim as runtime
COPY src/main/resources/conf_docker_mysql.xml conf_docker.xml
COPY --from=builder /build/target/*.jar app.jar
ENTRYPOINT ["java","-jar", "app.jar", "--app.configuration.file=conf_docker.xml"]