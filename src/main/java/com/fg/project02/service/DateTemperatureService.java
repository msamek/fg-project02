package com.fg.project02.service;

import com.fg.project02.adam.DateTemperatureDao;
import com.fg.project02.controller.model.DateTemperatureRequest;
import com.fg.project02.exception.DateTemperatureNotExist;
import com.fg.project02.model.DateTemperature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class DateTemperatureService {

    private DateTemperatureDao manager;

    @Autowired
    public DateTemperatureService(DateTemperatureDao manager) {
        this.manager = manager;
    }


    public List<DateTemperature> findAll(int pageNo, int pageSize) {
        return manager.getList();
    }

    public DateTemperature findById(Integer id) throws DateTemperatureNotExist {
        DateTemperature dateTemperatureById = manager.getDateTemperatureById(id);
        if (dateTemperatureById == null) {
            dateTemperatureNotExist(id);
        }

        return dateTemperatureById;
    }

    public DateTemperature addDateTemperature(DateTemperatureRequest request) {
        if (request != null) {
            DateTemperature aNew = manager.createNew();
            DateTemperature dateTemperature = DateTemperature.fromRequest(request, aNew);
            manager.store(dateTemperature);
            return dateTemperature;
        }

        return null;
    }

    public DateTemperature updateDateTemperature(Integer id, DateTemperatureRequest request) throws DateTemperatureNotExist {
        if (request == null) {
            return null;
        }

        DateTemperature dateTemperature = manager.getDateTemperatureById(id);
        if (dateTemperature == null) {
            dateTemperatureNotExist(id);
        }

        DateTemperature patch = DateTemperature.fromRequest(request, dateTemperature);;
        manager.store(patch);

        return patch;
    }

    public DateTemperature patchDateTemperature(Integer id, DateTemperatureRequest temp) throws DateTemperatureNotExist {
        DateTemperature originalTemp = manager.getDateTemperatureById(id);
        if (originalTemp == null) {
            dateTemperatureNotExist(id);
        }

        DateTemperature patch = originalTemp.patch(temp);
        manager.store(patch);

        return patch;
    }

    public void deleteDateTemperature(Integer id) throws DateTemperatureNotExist {
        if (manager.removeById(id) == 0) {
            dateTemperatureNotExist(id);
        }
    }

    private void dateTemperatureNotExist(Integer id) throws DateTemperatureNotExist {
        throw new DateTemperatureNotExist(String.format("Original temperature with id %d not found", id));
    }
}
