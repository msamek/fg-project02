package com.fg.project02.service;

import com.fg.project02.adam.DateTemperatureDao;
import com.fg.project02.exception.DateTemperatureNotExist;
import com.fg.project02.exception.WrongInterval;
import com.fg.project02.model.DateTemperature;
import com.fg.project02.model.TimeInterval;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TemperatureStatisticsService {
    private DateTemperatureDao manager;

    @Autowired
    public TemperatureStatisticsService(DateTemperatureDao dateTemperatureMapper) {
        this.manager = dateTemperatureMapper;
    }

    public TimeInterval calculateInterval(Float minTemperature, Float maxTemperature, Integer fromHour, Integer toHour) throws WrongInterval, DateTemperatureNotExist {
        List<DateTemperature> allOrderByDate = getDateTemperatures(fromHour, toHour);
        TimeInterval longestInterval = null;
        TimeInterval actualInterval = new TimeInterval();
        for (DateTemperature actualTemp : allOrderByDate) {
            if (actualTemp.inRange(minTemperature, maxTemperature)) {
                // Extend the interval
                setIntervalDate(actualInterval, actualTemp);
            } else if (actualInterval.isCalculated()) {
                // Calculate longest interval after at-least 1 date was found
                longestInterval = getLongestInterval(longestInterval, actualInterval);
                actualInterval = new TimeInterval();
            }
        }

        // Check if the longest interval was not on the end of array
        return longestInterval == null ? actualInterval : getLongestInterval(longestInterval, actualInterval);
    }

    private boolean isInsideInterval(DateTemperature dateTemperature, Integer fromHour, Integer toHour) {
        int hour = dateTemperature.getDate().getHour();
        return hour >= fromHour && hour <= toHour;
    }

    private TimeInterval getLongestInterval(TimeInterval longestInterval, TimeInterval actualInterval) {
        return longestInterval == null || actualInterval.getBetween() > longestInterval.getBetween() ? actualInterval : longestInterval;
    }

    private List<DateTemperature> getDateTemperatures(Integer fromHour, Integer toHour) throws WrongInterval, DateTemperatureNotExist {
        List<DateTemperature> dateTemperatures = fromHour != null || toHour != null ?
                getHourDateTemperatures(fromHour, toHour) :
                this.manager.getList().stream().sorted(Comparator.comparing(DateTemperature::getDate)).collect(Collectors.toList());
        if (dateTemperatures.isEmpty()) {
            throw new DateTemperatureNotExist("Date temperatures doesn't exist for selected interval");
        }

        return dateTemperatures;
    }

    private List<DateTemperature> getHourDateTemperatures(Integer fromHour, Integer toHour) throws WrongInterval {
        if (fromHour != null && toHour != null) {
            return this.manager.getList().stream().filter(a -> isInsideInterval(a, fromHour, toHour))
                    .sorted(Comparator.comparing(DateTemperature::getDate)).collect(Collectors.toList());
        }

        throw new WrongInterval("Both hour parameters must be set");
    }

    private void setIntervalDate(TimeInterval actualInterval, DateTemperature actualDate) {
        if (actualInterval.getFrom() == null) {
            actualInterval.setFrom(actualDate.getDate());
        }
        actualInterval.setTo(actualDate.getDate());
    }
}
