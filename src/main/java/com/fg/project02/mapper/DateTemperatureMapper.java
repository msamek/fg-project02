package com.fg.project02.mapper;

import com.fg.project02.model.DateTemperature;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface DateTemperatureMapper {

    @Select("SELECT * FROM datetime_temperature WHERE id = #{id}")
    DateTemperature findById(@Param("id") Integer id);

    @Select("SELECT * FROM datetime_temperature ORDER BY id")
    Page<DateTemperature> findAllPageableOrderById();

    @Select("SELECT * FROM datetime_temperature ORDER BY date ASC")
    List<DateTemperature> findAllOrderByDate();

    @Select("SELECT * FROM datetime_temperature WHERE hour(date) >= #{from} && hour(date) <= #{to} ORDER BY date ASC")
    List<DateTemperature> findAllInHourIntervalOrderByDate(@Param("from") Integer fromHour, @Param("to") Integer toHour);

    @Insert("INSERT INTO datetime_temperature (id, date, value) VALUES (null, #{date}, #{value})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    Integer insert(DateTemperature dateTemperature);

    @Update("UPDATE datetime_temperature SET date=#{date}, value = #{value} WHERE id = #{id}")
    Integer update(DateTemperature dateTemperature);

    @Delete("DELETE FROM datetime_temperature WHERE id = #{id}")
    Integer delete(@Param("id") Integer id);
}
