package com.fg.project02.controller.handler;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

@ControllerAdvice(annotations = RestController.class)
public class GlobalExceptionHandler {
    private static final HttpStatus DEFAULT_EXCEPTION_STATUS = HttpStatus.BAD_REQUEST;
    private static final String DEFAULT_MESSAGE = "Unexpected error";

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public ResponseEntity<ApiError> validationException(MethodArgumentNotValidException e) {
        return new ResponseEntity<>(new ApiError(DEFAULT_EXCEPTION_STATUS, "Validation exception", e,
                e.getBindingResult().getFieldErrors()
                        .stream().map(DefaultMessageSourceResolvable::getDefaultMessage)
                        .collect(java.util.stream.Collectors.joining(", "))),
                DEFAULT_EXCEPTION_STATUS);

    }

    @ExceptionHandler(Exception.class)
    @ResponseBody
    protected ResponseEntity<Object> defaultErrorHandler(Exception ex) {
        HttpStatus status = DEFAULT_EXCEPTION_STATUS;
        String message = DEFAULT_MESSAGE;
        ResponseStatus annotation = AnnotationUtils.findAnnotation(ex.getClass(), ResponseStatus.class);
        if (annotation != null) {
            status = annotation.value();
            message = !annotation.reason().isEmpty() ? annotation.reason() : message;
        }
        return new ResponseEntity<>(new ApiError(status, message, ex, null), status);
    }

    @Getter
    @Setter
    private static class ApiError {
        private HttpStatus status;
        private String message;
        private String debugMessage;
        private List<String> errors;

        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
        private LocalDateTime timestamp;

        public ApiError(final HttpStatus status, final String message, Throwable ex, final String error) {
            this.timestamp = LocalDateTime.now();
            this.status = status;
            this.message = message;
            this.debugMessage = ex.getLocalizedMessage();
            this.errors = Collections.singletonList(error);
        }
    }
}