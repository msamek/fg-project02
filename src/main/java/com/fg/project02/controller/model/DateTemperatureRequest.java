package com.fg.project02.controller.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fg.metadata.business.aop.AdamProxyAllMethods;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class DateTemperatureRequest {
    @NotNull(message = "Temperature cannot be null")
    @JsonProperty(value = "temperature")
    @Digits(fraction = 1, integer = 3)
    @DecimalMax("99.9")
    @DecimalMin("-273.15")
    private Float value;

    @NotNull(message = "Date cannot be null")
    @DateTimeFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime date;

    public DateTemperatureRequest(float v, LocalDateTime now) {
    }
}
