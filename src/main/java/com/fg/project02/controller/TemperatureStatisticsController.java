package com.fg.project02.controller;

import com.fg.project02.exception.DateTemperatureNotExist;
import com.fg.project02.exception.WrongInterval;
import com.fg.project02.model.TimeInterval;
import com.fg.project02.service.TemperatureStatisticsService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "v1/stats", produces = "application/json")
@Api(tags = "Temperature statistics Controller")
public class TemperatureStatisticsController {
    private TemperatureStatisticsService service;

    @Autowired
    public TemperatureStatisticsController(TemperatureStatisticsService service) {
        this.service = service;
    }

    @ApiOperation(value = "Retrieve a time interval for selected temperature and hour    boundaries", response = TimeInterval.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved a time interval"),
            @ApiResponse(code = 400, message = "Time interval not found"),
    })
    @GetMapping(path = "/calculate")
    public @ResponseBody
    TimeInterval calculate(
            @ApiParam(name = "min", value = "Minimal allowed temperature", required = true)
            @RequestParam(name = "min") Float minTemperature,
            @ApiParam(name = "max", value = "Maximal allowed temperature", required = true)
            @RequestParam(name = "max") Float maxTemperature,
            @ApiParam(name = "fromHour", value = "Measure daily temperatures from this hour")
            @RequestParam(required = false) Integer fromHour,
            @ApiParam(name = "toHour", value = "Measure daily temperatures to this hour")
            @RequestParam(required = false) Integer toHour) throws WrongInterval, DateTemperatureNotExist {
        return service.calculateInterval(minTemperature, maxTemperature, fromHour, toHour);
    }
}
