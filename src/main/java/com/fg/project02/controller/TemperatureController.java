package com.fg.project02.controller;

import com.fg.project02.controller.model.DateTemperatureRequest;
import com.fg.project02.exception.DateTemperatureNotExist;
import com.fg.project02.model.DateTemperature;
import com.fg.project02.service.DateTemperatureService;
import com.github.pagehelper.PageSerializable;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "v1/temperatures", produces = "application/json")
@Api(tags = "Date temperature Controller")
public class TemperatureController {

    private DateTemperatureService service;

    @Autowired
    public TemperatureController(DateTemperatureService service) {
        this.service = service;
    }

    @ApiOperation(value = "View a list of available date temperatures", response = DateTemperature.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list")
    })
    @GetMapping
    @ResponseBody
    public List<DateTemperature> getTemperatures(
            @ApiParam(name = "page", value = "Index of page", defaultValue = "0", required = false)
            @RequestParam(name = "page", required = false, defaultValue = "0") Integer pageNo,
            @ApiParam(name = "max", value = "Number of items on one page", defaultValue = "0", required = false)
            @RequestParam(name = "max", required = false, defaultValue = "0") Integer max) {
        return service.findAll(pageNo, max);
    }

    @ApiOperation(value = "Retrieve a date temperature by id", response = DateTemperature.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved a date temperature"),
            @ApiResponse(code = 400, message = "Date temperature not found"),
    })
    @GetMapping(path = "/{id}")
    @ResponseBody
    public DateTemperature getTemperature(
            @ApiParam(name = "id", value = "Id of requested date temperature", defaultValue = "1", required = true)
            @PathVariable Integer id) throws DateTemperatureNotExist {
        return service.findById(id);
    }

    @ApiOperation(value = "Add a new date temperature", response = DateTemperature.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully saved a new date temperature"),
            @ApiResponse(code = 400, message = "Temperature for selected date already exist"),
    })
    @PostMapping
    @ResponseBody
    public DateTemperature addTemperature(
            @ApiParam(name = "temperature", value = "Structure to create a new date temperature", required = true)
            @Valid @RequestBody DateTemperatureRequest temperature) {
        return service.addDateTemperature(temperature);
    }

    @ApiOperation(value = "Update a date temperature by id", response = DateTemperature.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated a date temperature"),
            @ApiResponse(code = 400, message = "Update of date temperature failed"),
    })
    @PutMapping(path = "/{id}")
    @ResponseBody
    public DateTemperature updateTemperature(
            @ApiParam(name = "id", value = "Id of date temperature to be updated", required = true)
            @PathVariable Integer id,
            @ApiParam(name = "temperature", value = "Structure to update date temperature", required = true)
            @Valid @RequestBody DateTemperatureRequest temperature) throws DateTemperatureNotExist {
        return service.updateDateTemperature(id, temperature);
    }

    @ApiOperation(value = "Partially update a date temperature by id", response = DateTemperature.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully patched a date temperature"),
            @ApiResponse(code = 400, message = "Patch of date temperature failed"),
    })
    @PatchMapping(path = "/{id}")
    @ResponseBody
    public DateTemperature patchTemperature(
            @ApiParam(name = "id", value = "Id of date temperature to be patched", required = true)
            @PathVariable Integer id,
            @ApiParam(name = "temperature", value = "Structure to patch date temperature")
            @RequestBody DateTemperatureRequest temperature) throws DateTemperatureNotExist {
        return service.patchDateTemperature(id, temperature);
    }

    @ApiOperation(value = "Removal a date temperature by id")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Successfully removed a date temperature"),
            @ApiResponse(code = 400, message = "Removal of date temperature failed"),
    })
    @DeleteMapping(path = "/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void removeTemperature(
            @ApiParam(name = "id", value = "Id of date temperature to be removed", required = true)
            @PathVariable Integer id) throws DateTemperatureNotExist {
        service.deleteDateTemperature(id);
    }
}
