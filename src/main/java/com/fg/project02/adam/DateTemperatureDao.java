package com.fg.project02.adam;

import com.fg.metadata.business.aop.dao.GenericDaoImpl;
import com.fg.project02.model.DateTemperature;

public abstract class DateTemperatureDao extends GenericDaoImpl<DateTemperature, Integer> {

    protected DateTemperatureDao() {
        super(DateTemperature.class);
    }

    public abstract DateTemperature getDateTemperatureById(int id);
}
