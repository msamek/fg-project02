package com.fg.project02.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fg.metadata.business.aop.AdamProxyAllMethods;
import com.fg.metadata.model.MetadataContainerProvider;
import com.fg.metadata.model.data.IdAccessor;
import com.fg.project02.controller.model.DateTemperatureRequest;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@AdamProxyAllMethods
@Data
public abstract class DateTemperature implements IdAccessor<Integer>, MetadataContainerProvider {

    private Integer id;

    @NotNull(message = "Temperature cannot be null")
    @JsonProperty(value = "temperature")
    @Digits(fraction = 1, integer = 3)
    @DecimalMax("99.9")
    @DecimalMin("-273.15")
    private Float value;

    @NotNull(message = "Date cannot be null")
    @DateTimeFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime date;


    public boolean inRange(Float minTemperature, Float maxTemperature) {
        return this.getValue() != null && this.getValue() >= minTemperature && this.getValue() <= maxTemperature;
    }

    public DateTemperature patch(DateTemperatureRequest temperature) {
        if (temperature.getDate() != null) {
            this.setDate(temperature.getDate());
        }

        if (temperature.getValue() != null) {
            this.setValue(temperature.getValue());
        }

        return this;
    }

    public static DateTemperature fromRequest(DateTemperatureRequest request, DateTemperature instance) {
        instance.setValue(request.getValue());
        instance.setDate(request.getDate());
        return instance;
    }
}
