package com.fg.project02.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class TimeInterval {
    private static final Long DEFAULT_BETWEEN = 0L;

    @DateTimeFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private LocalDateTime from;

    @DateTimeFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private LocalDateTime to;

    @JsonIgnore
    private Long between = DEFAULT_BETWEEN;

    public Long getBetween() {
        if (between == 0L && this.from != null && this.to != null) {
            this.between = Duration.between(this.from.toInstant(ZoneOffset.UTC), this.to.toInstant(ZoneOffset.UTC)).getSeconds();
        }
        return between;
    }

    public void setFrom(LocalDateTime from) {
        this.between = DEFAULT_BETWEEN;
        this.from = from;
    }

    public void setTo(LocalDateTime to) {
        this.between = DEFAULT_BETWEEN;
        this.to = to;
    }

    @JsonIgnore
    public boolean isCalculated() {
        return this.from != null && this.to != null;
    }
}

