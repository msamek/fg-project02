package com.fg.project02.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Date temperature not found.")
public class DateTemperatureNotExist extends Exception {

    public DateTemperatureNotExist(String message) {
        super(message);
    }
}
