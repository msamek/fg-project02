package com.fg.project02.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Date interval is wrong")
public class WrongInterval extends Exception {

    public WrongInterval(String message) {
        super(message);
    }
}
