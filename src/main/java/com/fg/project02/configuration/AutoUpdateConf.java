package com.fg.project02.configuration;

import com.fg.autoupdate.AutoUpdater;
import com.fg.autoupdate.AutoUpdaterBuilder;
import com.fg.autoupdate.AutoUpdaterConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(AutoUpdaterConfiguration.class)
public class AutoUpdateConf {

    @Bean(name = "autoUpdate")
    public AutoUpdater getDatabaseAutoUpdated(@Autowired ApplicationContext applicationContext) {
        return new AutoUpdaterBuilder(applicationContext, "date_temperature", "1.0")
                .withSkipIfDataSourceNotPresent(true)
                .withResourcePath("classpath:/sql/")
                .build();
    }

}