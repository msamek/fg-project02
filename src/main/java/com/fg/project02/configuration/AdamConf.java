package com.fg.project02.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fg.metadata.business.MetadataManager;
import com.fg.metadata.business.MetadataManagerBuilder;
import com.fg.metadata.converter.*;
import com.fg.metadata.model.data.util.CustomJsonVisibilityChecker;
import com.fg.project02.adam.DateTemperatureDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

@Configuration
public class AdamConf{

    @Bean
    @DependsOn("autoUpdate")
    public MetadataManager getMetadataManager(@Autowired ApplicationContext applicationContext) {
        return new MetadataManagerBuilder(applicationContext)
                .withMetadataConfiguration(new ClassPathResource("metadata/metadata-configuration.xml"))
                .withConverters(
                        new TimestampToLocalDateTimeConverter(),
                        new SqlDateToLocalDateConverter(),
                        new LocalDateTimeToSqlDateConverter(),
                        new LocalDateToSqlDateConverter(),
                        new LocalDateTimeToTimestampConverter()
                )
                .createMetadataManager();
    }

    @Bean
    public DateTemperatureDao getDateTemperatureDao(@Autowired MetadataManager metadataManager) {
        return metadataManager.createGenericDaoImplementation(DateTemperatureDao.class);
    }

    @Bean
    public ObjectMapper getObjectMapper() {
        MappingJackson2HttpMessageConverter jacksonMessageConverter = new MappingJackson2HttpMessageConverter();
        ObjectMapper objectMapper = jacksonMessageConverter.getObjectMapper();
        objectMapper.setVisibility(new CustomJsonVisibilityChecker(false, false));
        return objectMapper;
    }
}
