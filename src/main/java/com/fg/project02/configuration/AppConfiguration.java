package com.fg.project02.configuration;

import org.apache.ibatis.session.SqlSessionFactory;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.boot.autoconfigure.SpringBootVFS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

@Configuration
@MapperScan("com.fg.project02.mapper")
public class AppConfiguration {
    private static final String DEFAULT_CON_STRING = "jdbc:mysql://localhost:3306/project01";
    private static final String DEFAULT_USER = "root";
    private static final String DEFAULT_PASS = "";
    private static final Logger LOG = LoggerFactory.getLogger(AppConfiguration.class);

    @Value("${app.configuration.file}")
    private String customConfFile;
    private Element xmlRoot;

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfig() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @PostConstruct
    private void loadConfiguration() throws JDOMException, IOException {
        SAXBuilder saxBuilder = new SAXBuilder();
        LOG.info("Trying to load with configuration file: {}", customConfFile);
        boolean exists = new File(customConfFile).exists();
        LOG.info("File exist: {}", exists);
        if (exists) {
            Document document = saxBuilder.build(new FileReader(customConfFile));
            this.xmlRoot = document.getRootElement();
            LOG.info("Configuration file loaded");
        }
    }

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        LOG.info("Database connection:" + getElementValue("connectionstring", DEFAULT_CON_STRING));
        dataSource.setUrl(getElementValue("connectionstring", DEFAULT_CON_STRING));
        dataSource.setUsername(getElementValue("username", DEFAULT_USER));
        dataSource.setPassword(getElementValue("password", DEFAULT_PASS));

        return dataSource;
    }

    @Bean
    public SqlSessionFactory masterSqlSessionFactory() throws Exception {
        SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
        factoryBean.setDataSource(dataSource());
        factoryBean.setVfs(SpringBootVFS.class);
        return factoryBean.getObject();
    }

    private String getElementValue(String key, String defaultValue) {
        return xmlRoot != null && xmlRoot.getChild(key) != null ? xmlRoot.getChild(key).getValue() : defaultValue;
    }
}
