create table datetime_temperature
(
	id int auto_increment
		primary key,
	date datetime not null,
	value decimal(4,1) not null,
	constraint datetime_temperature_date_uindex
		unique (date)
);

