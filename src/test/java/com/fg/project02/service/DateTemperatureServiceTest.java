package com.fg.project02.service;

import com.fg.project02.adam.DateTemperatureDao;
import com.fg.project02.controller.model.DateTemperatureRequest;
import com.fg.project02.exception.DateTemperatureNotExist;
import com.fg.project02.model.DateTemperature;
import lombok.SneakyThrows;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class DateTemperatureServiceTest {
    @Mock
    private DateTemperatureDao dateTemperatureDao;

    @InjectMocks
    private DateTemperatureService service;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }


    @SneakyThrows
    @Test(expected = DateTemperatureNotExist.class)
    public void FindById_ParamNotExist_ThrowsDateTemperatureNotExistException() {
        when(this.dateTemperatureDao.getDateTemperatureById(13)).thenReturn(null);
        this.service.findById(13);
    }

    @SneakyThrows
    @Test
    public void FindById_ParamExist_ReturnsObject() {
        DateTemperature expected = createTemperature(50f, LocalDateTime.now());
        when(this.dateTemperatureDao.getDateTemperatureById(13)).thenReturn(expected);
        DateTemperature actual = this.service.findById(13);

        assertEquals(expected, actual);
    }

    @Test
    public void AddDateTemperature_NullParam_ReturnsNull() {
        DateTemperature actual = this.service.addDateTemperature(null);

        assertNull(actual);
        verify(this.dateTemperatureDao, times(0)).store(null);
    }

    @Test
    public void AddDateTemperature_ValidParam_ReturnsObject() {
        DateTemperature expected = createTemperature(50f, LocalDateTime.now());
        when(this.dateTemperatureDao.store(expected)).thenReturn(true);

        DateTemperature actual = this.service.addDateTemperature(new DateTemperatureRequest(50f, LocalDateTime.now()));

        assertEquals(expected, actual);
        verify(this.dateTemperatureDao, times(1)).store(expected);
    }

    @Test(expected = DateTemperatureNotExist.class)
    public void UpdateDateTemperature_ParamNotFound_ThrowsException() throws DateTemperatureNotExist {
        DateTemperature expected = createTemperature(50f, LocalDateTime.now());
        when(this.dateTemperatureDao.store(expected)).thenReturn(false);

        DateTemperature actual = this.service.updateDateTemperature(1, new DateTemperatureRequest(50f, LocalDateTime.now()));

        assertEquals(expected, actual);
        verify(this.dateTemperatureDao, times(1)).store(expected);
    }


    @SneakyThrows
    @Test
    public void UpdateDateTemperature_ValidParam_ReturnsObject() {
        DateTemperature expected = createTemperature(50f, LocalDateTime.now());
        expected.setId(1);

        when(this.dateTemperatureDao.store(any())).thenReturn(true);
        when(this.dateTemperatureDao.getDateTemperatureById(1)).thenReturn(expected);

        DateTemperature actual = this.service.updateDateTemperature(1, new DateTemperatureRequest(50f, LocalDateTime.now()));

        assertEquals(expected, actual);
        verify(this.dateTemperatureDao, times(1)).store(expected);
    }

    @SneakyThrows
    @Test
    public void PatchDateTemperature_ValidParam_ReturnsObject() {
        DateTemperature expected = createTemperature(50f, LocalDateTime.now());
        expected.setId(13);
        DateTemperature patch = createTemperature(50f, null);
        patch.setId(13);
        DateTemperature original = createTemperature(30f, LocalDateTime.now());
        original.setId(13);
        DateTemperatureRequest temp = new DateTemperatureRequest(50f, LocalDateTime.now());
        when(patch.patch(temp)).thenReturn(expected);
        when(this.dateTemperatureDao.getDateTemperatureById(13)).thenReturn(patch);
        when(this.dateTemperatureDao.store(expected)).thenReturn(true);


        DateTemperature actual = this.service.patchDateTemperature(13, temp);

        assertEquals(expected, actual);
        verify(this.dateTemperatureDao, times(1)).store(expected);
        verify(this.dateTemperatureDao, times(1)).getDateTemperatureById(13);
    }

    @SneakyThrows
    @Test
    public void DeleteDateTemperature_ValidParam_ReturnsObject() {
        when(this.dateTemperatureDao.removeById(13)).thenReturn(1);

        this.service.deleteDateTemperature(13);

        verify(this.dateTemperatureDao, times(1)).removeById(13);
    }

    @Test(expected = DateTemperatureNotExist.class)
    public void DeleteDateTemperature_WrongParam_ThrowsException() throws DateTemperatureNotExist {
        when(this.dateTemperatureDao.removeById(13)).thenReturn(0);

        this.service.deleteDateTemperature(13);

        verify(this.dateTemperatureDao, times(1)).removeById(13);
    }

    private DateTemperature createTemperature(Float value, LocalDateTime time) {
        when(dateTemperatureDao.createNew()).thenReturn(Mockito.mock(DateTemperature.class, Mockito.CALLS_REAL_METHODS));
        DateTemperature aNew = dateTemperatureDao.createNew();
        when(aNew.getValue()).thenReturn(value);
        when(aNew.getDate()).thenReturn(time);
        return aNew;
    }


}
