package com.fg.project02.service;

import com.fg.project02.adam.DateTemperatureDao;
import com.fg.project02.exception.DateTemperatureNotExist;
import com.fg.project02.exception.WrongInterval;
import com.fg.project02.model.DateTemperature;
import com.fg.project02.model.TimeInterval;
import lombok.SneakyThrows;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TemperatureStatisticsServiceTest {

    @Mock
    private DateTemperatureDao dateTemperatureDao;

    @InjectMocks
    private TemperatureStatisticsService service;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @SneakyThrows
    @Test(expected = DateTemperatureNotExist.class)
    public void CalculateInterval_EmptyTemperatures_RaisesDateTemperatureNotExistException() {
        when(this.dateTemperatureDao.getList()).thenReturn(new ArrayList<>());
        TimeInterval expectedInterval = new TimeInterval();
        TimeInterval actualInterval = this.service.calculateInterval(0F, 100F, null, null);

        verify(this.dateTemperatureDao, times(1)).getList();
        assertEquals(expectedInterval, actualInterval);
    }

    @SneakyThrows
    @Test
    public void CalculateInterval_OneTemperature_ReturnsInterval() {
        ArrayList<DateTemperature> t = new ArrayList<>();
        LocalDateTime now = LocalDateTime.now();
        t.add(createTemperature(50F, now));
        when(this.dateTemperatureDao.getList()).thenReturn(t);
        TimeInterval expectedInterval = createTimeInterval(now, now);

        TimeInterval actualInterval = this.service.calculateInterval(0F, 100F, null, null);

        verify(this.dateTemperatureDao, times(1)).getList();
        assertEquals(expectedInterval, actualInterval);
    }

    @SneakyThrows
    @Test
    public void CalculateInterval_OneTemperatureOutside_ReturnsEmptyInterval() {
        ArrayList<DateTemperature> t = new ArrayList<>();
        LocalDateTime now = LocalDateTime.now();
        t.add(createTemperature(50F, now));
        when(this.dateTemperatureDao.getList()).thenReturn(t);
        TimeInterval expectedInterval = createTimeInterval(null, null);

        TimeInterval actualInterval = this.service.calculateInterval(0F, 10F, null, null);

        verify(this.dateTemperatureDao, times(1)).getList();
        assertEquals(expectedInterval, actualInterval);
    }

    @Test
    public void CalculateInterval_MultipleDates_ReturnsInterval() throws WrongInterval, DateTemperatureNotExist {
        ArrayList<DateTemperature> t = new ArrayList<>();
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime tenMinAgo = now.minusMinutes(10);
        LocalDateTime yesterday = now.minusDays(1);
        t.add(createTemperature(5F, yesterday));
        t.add(createTemperature(5F, tenMinAgo));
        t.add(createTemperature(100F, now));
        when(this.dateTemperatureDao.getList()).thenReturn(t);
        TimeInterval expectedInterval = createTimeInterval(yesterday, tenMinAgo);

        TimeInterval actualInterval = this.service.calculateInterval(0F, 10F, null, null);

        verify(this.dateTemperatureDao, times(1)).getList();
        assertEquals(expectedInterval, actualInterval);
    }

    @Test
    public void CalculateInterval_MultipleDatesTwoIntervals_ReturnsInterval() throws WrongInterval, DateTemperatureNotExist {
        ArrayList<DateTemperature> t = new ArrayList<>();
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime tenMinAgo = now.minusMinutes(10);
        LocalDateTime yesterday = now.minusDays(1);
        LocalDateTime tenMinFuture = now.plusMinutes(10);
        t.add(createTemperature(5f, yesterday));
        t.add(createTemperature(100f, tenMinAgo));
        t.add(createTemperature(5f, now));
        t.add(createTemperature(5f, tenMinFuture));
        when(this.dateTemperatureDao.getList()).thenReturn(t);
        TimeInterval expectedInterval = createTimeInterval(now, tenMinFuture);

        TimeInterval actualInterval = this.service.calculateInterval(0F, 10F, null, null);

        verify(this.dateTemperatureDao, times(1)).getList();
        assertEquals(expectedInterval, actualInterval);
    }

    @Test(expected = WrongInterval.class)
    public void CalculateInterval_OneNullHourParams_ThrowsWrongIntervalException() throws WrongInterval, DateTemperatureNotExist {
        when(this.dateTemperatureDao.getList()).thenReturn(new ArrayList<>());
        TimeInterval expectedInterval = new TimeInterval();
        TimeInterval actualInterval = this.service.calculateInterval(0F, 100F, 11, null);

        verify(this.dateTemperatureDao, times(1)).getList();
        assertEquals(expectedInterval, actualInterval);
    }

    @SneakyThrows
    @Test(expected = DateTemperatureNotExist.class)
    public void CalculateInterval_EmptyTemperaturesWithHours_RaisesDateTemperatureNotExistException() {
        when(this.dateTemperatureDao.getList()).thenReturn(new ArrayList<>());
        TimeInterval expectedInterval = new TimeInterval();
        TimeInterval actualInterval = this.service.calculateInterval(0F, 100F, 11, 12);

        verify(this.dateTemperatureDao, times(1)).getList();
        assertEquals(expectedInterval, actualInterval);
    }


    @Test
    public void CalculateInterval_OneTemperatureCorrectHour_ReturnsInterval() throws WrongInterval, DateTemperatureNotExist {
        ArrayList<DateTemperature> t = new ArrayList<>();
        LocalDateTime date = createDate("1986-04-08 11:30");
        t.add(createTemperature(50f, date));
        when(this.dateTemperatureDao.getList()).thenReturn(t);
        TimeInterval expectedInterval = createTimeInterval(date, date);

        TimeInterval actualInterval = this.service.calculateInterval(0F, 100F, 11, 12);

        verify(this.dateTemperatureDao, times(1)).getList();
        assertEquals(expectedInterval, actualInterval);
    }

    private LocalDateTime createDate(String str) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        return LocalDateTime.parse(str, formatter);
    }

    private DateTemperature createTemperature(Float value, LocalDateTime time) {
        when(dateTemperatureDao.createNew()).thenReturn(Mockito.mock(DateTemperature.class, Mockito.CALLS_REAL_METHODS));
        DateTemperature aNew = dateTemperatureDao.createNew();
        when(aNew.getValue()).thenReturn(value);
        when(aNew.getDate()).thenReturn(time);

        return aNew;
    }

    private TimeInterval createTimeInterval(LocalDateTime from, LocalDateTime to) {
        TimeInterval expectedInterval = new TimeInterval();
        expectedInterval.setFrom(from);
        expectedInterval.setTo(to);
        expectedInterval.getBetween(); //calculate between
        return expectedInterval;
    }

}
