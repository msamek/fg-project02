package com.fg.project02.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class DateTemperatureTest {

    @Test
    public void InRange_CorrectParam_ReturnTrue() {
//        DateTemperature dateTemperature = new DateTemperature(50f, LocalDateTime.now());

//        assertTrue(dateTemperature.inRange(0f, 100f));
    }

    @Test
    public void InRange_InvalidParamLessTemperature_ReturnFalse() {
//        DateTemperature dateTemperature = new DateTemperature(5f, LocalDateTime.now());

//        assertFalse(dateTemperature.inRange(10f, 100f));
    }

    @Test
    public void InRange_InvalidParaMoreTemp_ReturnTrue() {
//        DateTemperature dateTemperature = new DateTemperature(105f, LocalDateTime.now());

//        assertFalse(dateTemperature.inRange(10f, 100f));
    }


}
