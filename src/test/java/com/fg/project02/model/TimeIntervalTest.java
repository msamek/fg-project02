package com.fg.project02.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;

import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TimeIntervalTest {

    @Test
    public void Between_SetFrom_EqualsDefaultValue() {
        TimeInterval timeInterval = new TimeInterval();
        timeInterval.setBetween(555L);
        timeInterval.setFrom(LocalDateTime.now());

        assertTrue(timeInterval.getBetween() == 0L);
    }

    @Test
    public void Between_SetTo_EqualsDefaultValue() {
        TimeInterval timeInterval = new TimeInterval();
        timeInterval.setBetween(555L);
        timeInterval.setTo(LocalDateTime.now());

        assertTrue(timeInterval.getBetween() == 0L);
    }

}
