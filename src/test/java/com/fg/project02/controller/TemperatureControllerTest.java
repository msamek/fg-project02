package com.fg.project02.controller;

import com.fg.project02.adam.DateTemperatureDao;
import com.fg.project02.model.DateTemperature;
import com.fg.project02.service.DateTemperatureService;
import com.github.pagehelper.PageSerializable;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.ArrayList;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TemperatureControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DateTemperatureService service;

    @MockBean
    private DateTemperatureDao manager;

    @Test
    public void GetTemperatures_NoParam_ReturnsCollection() throws Exception {

    }

    //TODO tests

}
