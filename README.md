# Second exercise - CRUD Application in Docker

**Running Application**:
mvn spring-boot:run (spring-boot:run -Drun.arguments=--key_param=value_param)

**Testing application**:
mvn test

#### **Running Application in docker**
**Running MySQL Container + Application container:** docker-compose up

**Running Integration tests**: cd newman && docker-compose up *(Applicable by default only after application is runnning in container)*

#### **CLI Parameters**
Parameter to override application.properties configuration file

    --app.configuration.file=#REPLACEME#

#### **Configuration file template**
Configuration file for JDBC configuration (MySQL Example)

    <?xml version="1.0" encoding="UTF-8"?>
    <configuration>
        <dbType>relation</dbType>
        <connectionstring>jdbc:mysql://localhost:3306/#SCHEMA-REPLACEME#</connectionstring>
        <username>#REPLACEME#</username>
        <password>#REPLACEME#</password>
    </configuration>


Configuration file for default Docker

    <?xml version="1.0" encoding="UTF-8"?>
    <configuration>
        <connectionstring>jdbc:mysql://docker-mysql:3306/project02</connectionstring>
        <username>root</username>
        <password>root</password>
    </configuration>


### API Documentation
API is documented in SWAGGER and is accessible via: http://localhost:5555/api/swagger-ui.html

### Database
Database DDL SQL is stored in the **/sql** folder

The DDL scripts has been created for MySQL

### Integration testing

Integration tests are written in Postman with 2 environments created

To execute Integration test without docker:
* Install Node
* npm install -G newman
* Execute:
  newman run FG_Project02.postman_collection.json
  -e Localhost.postman_environment.json
  -r cli
